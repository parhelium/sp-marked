Package.describe({
  summary: "Marked package"
});

Package.on_use(function (api, where) {
  api.add_files('lib/marked.js', ['client']);
  api.export([
     'marked',
  ], 'client');
});

